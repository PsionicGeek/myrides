import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:my_rides/routes/app_routes.dart';

class SplashController extends GetxController{
  final getStorage=GetStorage();
  @override
  void onReady() {
    
   
    
    super.onReady();
       if (getStorage.read("id") != null) {
      Future.delayed(const Duration(microseconds: 2000), () {
        Get.offAllNamed(Routes.HOME);
      });
    } else {
      Get.offAllNamed(Routes.LOGIN);
    }
  
  }
}