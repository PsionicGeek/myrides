import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../routes/app_routes.dart';

class AuthController extends GetxController {
  static AuthController authControllerInstance = Get.find<AuthController>();
  late Rx<User?> _user;
  FirebaseAuth auth = FirebaseAuth.instance;
  final getStorage = GetStorage();

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    _user = Rx<User?>(auth.currentUser);
    _user.bindStream(auth
        .userChanges()); //whenever there is a change in auth(Constumer) _user will get notified
    ever(_user, _intialScreen); //when ever _userchange intialScreen get called
  }

  _intialScreen(User? user) {
    if (user == null) {
      print("To Login Page");
      Get.offAllNamed(Routes.LOGIN);
    } else {
      Get.offAllNamed(Routes.HOME);
    }
  }

  void register(String email, password) async {
    try {
      await auth
          .createUserWithEmailAndPassword(email: email, password: password)
          .whenComplete(() {
        getStorage.write('id', 1);
        print("registration completed");
      });
    } catch (e) {
      Get.snackbar(
        "About User Registration",
        "User Message",
        snackPosition: SnackPosition.BOTTOM,
        titleText: Text("Account Creation Failed"),
        messageText: Text(e.toString()),
      );
    }
  }

  void login(String email, password) async {
    try {
      print("Yes");
      await auth
          .signInWithEmailAndPassword(email: email, password: password)
          .whenComplete(() {
        getStorage.write('id', 1);
        print("Login completed");
      });
    } catch (e) {
      Get.snackbar(
        "About User Registration",
        "User Message",
        snackPosition: SnackPosition.BOTTOM,
        titleText: Text("Account Creation Failed"),
        messageText: Text(e.toString()),
      );
    }
  }

  void logOut() async {
    try {
      await getStorage.write('id', null);
      await getStorage.erase();
      await auth.signOut();
      Get.offAllNamed(Routes.LOGIN);
    } catch (e) {
      Get.snackbar(
        "About User Registration",
        "User Message",
        snackPosition: SnackPosition.BOTTOM,
        titleText: Text("Account Creation Failed"),
        messageText: Text(e.toString()),
      );
    }
  }
}
