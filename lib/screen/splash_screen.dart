import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_rides/controller/splash_controller.dart';

class SplashScreen extends StatelessWidget {
SplashScreen({ Key? key }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Splash Screen"),
      ),
      body: Container(
        child: CircularProgressIndicator(
          color: Colors.black,
        ),
      ),
    );
  }
}