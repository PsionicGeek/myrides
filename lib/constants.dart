import 'package:flutter/material.dart';

const Color kLightBlack = Color(0xFF757575);
const Color kRedColor = Color(0xFFE8625D);
const Color kGreenColor = Color(0xFF025B5D);
const Color kLightGreenColor = Color(0xFF4DAF00);
const Color kLightTextColor = Color(0xFF636363);
const Color kBlueButtonColor = Color(0xFF00A3FF);
const Color kBgColor = Color(0xFFE5E5E5);
