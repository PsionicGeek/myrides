import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:my_rides/routes/app_routes.dart';
import 'package:my_rides/screen/splash_screen.dart';
import 'package:my_rides/screens/login_page.dart';
import 'package:my_rides/screens/register_page.dart';
import 'package:my_rides/screens/welcome_page.dart';

class AppPages {
  AppPages._();
  static const INITIAL = Routes.ROOT;
  static final routes = [
    GetPage(name: Routes.ROOT, page: () => SplashScreen()),
    GetPage(name: Routes.LOGIN, page: () => LoginPage()),
    GetPage(name: Routes.REGISTER, page: () => RegisterPage()),
    GetPage(name: Routes.HOME, page: () => WelcomePage())
  ];
}
