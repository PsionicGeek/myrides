abstract class Routes {
  Routes._();
  static const ROOT = '/';
  static const HOME = _Path.HOME;
  static const LOGIN = _Path.LOGIN;
  static const REGISTER = _Path.REGISTER;
}

abstract class _Path {
  static const ROOT = '/';
  static const HOME = '/welcomeScreen';
  static const LOGIN = '/loginpage';
  static const REGISTER = '/registrationPage';
}
