import 'package:get/get.dart';
import 'package:my_rides/controller/auth_controller.dart';
import 'package:my_rides/controller/splash_controller.dart';

class ControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(SplashController());
    Get.put(AuthController());
  }
}
