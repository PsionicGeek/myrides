import 'package:flutter/material.dart';
import 'package:my_rides/controller/auth_controller.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        children: [
          Center(
            child: Text("Welcome"),
          ),
          ElevatedButton(
              onPressed: () {
                AuthController.authControllerInstance.logOut();
                //Get.toNamed('/');
              },
              child: Text("Log Out"))
        ],
      ),
    ));
  }
}
